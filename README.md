# Flectra Community / connector-telephony

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[connector_voicent](connector_voicent/) | 2.0.1.0.0| Connect Odoo with Voicent
[asterisk_click2dial](asterisk_click2dial/) | 2.0.2.0.0| Asterisk-Odoo connector
[hr_phone](hr_phone/) | 2.0.1.0.0| Validate phone numbers in HR
[sms_no_alter_body](sms_no_alter_body/) | 2.0.1.0.0| Avoid sms formatting between html and text
[crm_phone](crm_phone/) | 2.0.1.1.0| Improve phone support in CRM
[sms_no_automatic_delete](sms_no_automatic_delete/) | 2.0.1.1.0| Avoid automatic delete of sended sms
[hr_recruitment_phone](hr_recruitment_phone/) | 2.0.1.0.0| Validate phone numbers in HR Recruitment
[event_phone](event_phone/) | 2.0.1.0.0| Validate phone numbers in Events
[base_phone](base_phone/) | 2.0.1.0.0| Validate phone numbers
[sms_ovh_http](sms_ovh_http/) | 2.0.1.0.3| Send sms using ovh http API


